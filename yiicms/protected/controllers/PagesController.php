<?php

class PagesController extends Controller
{
    
    	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
		);
	}
        
	public function actionIndex($id)
	{
            $models = Pages::model()->findAllByAttributes(array('categories_id' => $id));
            $category = Categories::model()->findByPk($id);
            $this->render('index', array('models' => $models, 'category' => $category));
	}

        public function actionView($id)
	{            
            $model = Pages::model()->findByPk($id);
            $newComment = new Comments;
            if(Yii::app()->user->isGuest)
                $newComment->scenario = 'Guest';           
            
            if(isset($_POST['Comments']))
            {
                $newComment->attributes=$_POST['Comments'];
                $newComment->pages_id=$model->id;
                
                $settings = Settings::model()->findByPk(1);
                if($settings->defaultStatusComment == 0){
                    $newComment->status = 0;
                }else{
                    $newComment->status = 1;
                }                
                
                
                if($newComment->save()){
                    
                    if($settings->defaultStatusComment == 0){
                        Yii::app()->user->setFlash('comment','Ждите подтверждение комментария.');
                    }else{
                        Yii::app()->user->setFlash('comment','Ваш комментарий опубликован.');
                    }                           
                    $this->refresh();
                }
            }            
            
 
            
            $this->render('view', array('model' => $model, 'newComment' => $newComment));
	}
}