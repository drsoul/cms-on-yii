<?php

/**
 * This is the model class for table "cms_comments".
 *
 * The followings are the available columns in table 'cms_comments':
 * @property integer $id
 * @property string $content
 * @property integer $pages_id
 * @property string $created
 * @property integer $users_id
 * @property string $guest
 */
class Comments extends CActiveRecord
{
        public $verifyCode;
        
        
        public static function all($pages_id){
            
            $criteria = new CDbCriteria;
            $criteria->compare('page_id', $page_id);
            $criteria->compare('status', 1);
            $criteria->order = 'created DESC';
            
            return new CActiveDataProvider('Comments', array('criteria'=>$criteria));           
        }
        
        public function beforeSave() {
            if($this->isNewRecord){
                $this->created = time();
            }
            
            if(!Yii::app()->user->isGuest)
                    $this->users_id = Yii::app()->user->id;
            
            return parent::beforeSave();
        }
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cms_comments';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
                        array('content', 'required'),
                        array('content, guest', 'required', 'on' => 'Guest'),
			//array('content, pages_id, created, users_id, guest', 'required'),
			array('pages_id, users_id', 'numerical', 'integerOnly'=>true),
			array('guest', 'length', 'max'=>15),
                        array('content', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, content, pages_id, created, users_id, guest, status', 'safe', 'on'=>'search'),
                        array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements(), 'on' => 'Guest'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'users' => array(self::BELONGS_TO, 'Users','users_id'),
            'pages' => array(self::BELONGS_TO, 'Pages','pages_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'content' => 'Содержимое',
			'pages_id' => 'Страница',
			'created' => 'Создано',
			'users_id' => 'Пользователь',
			'guest' => 'Имя (гость)',
            'status' => 'Статус',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('pages_id',$this->pages_id);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('users_id',$this->users_id);
		$criteria->compare('guest',$this->guest,true);
                $criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Comments the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
