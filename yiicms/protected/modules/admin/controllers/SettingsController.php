<?php

class SettingsController extends Controller
{
 
    
	public function actionIndex()
	{
		$model=Settings::model()->findByPk(1);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Settings']))
		{
			$model->attributes=$_POST['Settings'];
			if($model->save()){
	           Yii::app()->user->setFlash('settings','Настройки сохранены.');
			}
				
		}

		$this->render('index',array(
			'model'=>$model,
		));
	}   
    
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}
        
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index'),
				'roles'=>array('2'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}   
        
}