<?php
/* @var $this UsersController */
/* @var $model Users */



$this->menu=array(	
	array('label'=>'Создать пользователя', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#users-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Управление пользователями</h1>


<?php echo CHtml::link('Расширенный поиск','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php 
echo CHtml::form();
echo CHtml::submitButton('Разбанить', array('name'=>'noban'));
echo CHtml::submitButton('Бан', array('name'=>'ban'));
echo '<br>';
echo CHtml::submitButton('Админ', array('name'=>'admin'));
echo CHtml::submitButton('Не админ', array('name'=>'noadmin'));
?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'users-grid',
	'dataProvider'=>$model->search(),
        'selectableRows'=>2,
	'filter'=>$model,
	'columns'=>array(
		'id',
                array(
                    'class'=>'CCheckBoxColumn',
                    'id'=>'Users_id'
                ),
		'username',
		'password',
                'email',
                'created' => array(
                    'name' => 'created',
                    'value' => 'date("j.m.Y H:i", $data->created)',
                    'filter' => false,
                ),
                'ban' => array(
                    'name' => 'ban',
                    'value' => '($data->ban == 1)?"Бан":"Чист"',
                    'filter' => array('0'=>'Чист','1'=>'Бан'),
                ),
                'role' => array(
                    'name' => 'role',
                    'value' => '($data->role == 1)?"Пользователь":"Админ"',
                    'filter' => array('2'=>'Админ','1'=>'Пользователь'),
                ),        
                array(
                        'class'=>'CButtonColumn',
                ),
	),
)); ?>
<?php 
echo CHtml::endForm();
?>
