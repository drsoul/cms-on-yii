<?php
/* @var $this UsersController */
/* @var $model Users */


$this->menu=array(
	array('label'=>'Управление пользователями', 'url'=>array('index')),
	array('label'=>'Обновить пользователя', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Удалить пользователя', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Вы уверены?')),
);
?>

<h1>Просмотр пользователя #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'username',
                'email',
		'password',
		'created' => array(
                    'name' => 'created',
                    'value' => date("j.m.Y H:i", $model->created),
                ),
                'ban' => array(
                    'name' => 'ban',
                    'value' => ($model->ban == 1)?"Бан":"Чист",                    
                ),
                'role' => array(
                    'name' => 'role',
                    'value' => ($model->role == 1)?"Пользователь":"Админ",                    
                ),    
	),
)); ?>
