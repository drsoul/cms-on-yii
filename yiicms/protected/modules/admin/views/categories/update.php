<?php
/* @var $this CategoriesController */
/* @var $model Categories */


$this->menu=array(
	array('label'=>'Управление категориями', 'url'=>array('index')),
	array('label'=>'Создать категорию', 'url'=>array('create')),	
);
?>

<h1>Update Categories <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>