<?php
/* @var $this CommentsController */
/* @var $model Comments */


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#comments-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Управление комментариями</h1>

<?php echo CHtml::link('Расширенный поиск','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php 
echo CHtml::form();
echo CHtml::submitButton('Подтвердить', array('name'=>'noban'));
echo CHtml::submitButton('Скрыть', array('name'=>'ban'));
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'comments-grid',
	'dataProvider'=>$model->search(),
        'selectableRows'=>2,
	'filter'=>$model,
	'columns'=>array(
		'id',
                array(
                    'class'=>'CCheckBoxColumn',
                    'id'=>'post_id'
                ),
		'content',
		'pages_id' => array(
                    'name' => 'pages_id',
                    'value' => '$data->pages->title',
                    'filter' => Pages::all(),
                ),
                'created' => array(
                    'name' => 'created',
                    'value' => 'date("j.m.Y H:i", $data->created)',
                    'filter' => false,
                ),
                    'users_id' => array(
                    'name' => 'users_id',
                    'value' => '$data->users->username',
                    'filter' => Users::all(),
                ),
                    'status' => array(
                    'name' => 'status',
                    'value' => '($data->status == 1)?"Доступно":"Скрыто"',
                    'filter' => array('0'=>'Скрыто','1'=>'Доступно'),
                ), 
                'guest',
		array(
                    'class'=>'CButtonColumn',
                    'updateButtonOptions' => array('style'=>'display:none'),
		),
	),
)); ?>

<?php 
echo CHtml::endForm();
?>