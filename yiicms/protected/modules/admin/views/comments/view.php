<?php
/* @var $this CommentsController */
/* @var $model Comments */

$this->menu=array(
	array('label'=>'Управлять комментариями', 'url'=>array('index')),	
	array('label'=>'Удалить комментарий', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<h1>View Comments #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'content',		
            	'pages_id' => array(
                    'name' => 'pages_id',
                    'value' => $model->pages->title,
                ),
		'created' => array(
                    'name' => 'created',
                    'value' => date("j.m.Y H:i", $model->created),
                ),
		'users_id' => array(
                    'name' => 'users_id',
                    'value' => $model->users->username,
                ),
		'guest',
	),
)); ?>
