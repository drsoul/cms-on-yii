<?php
/* @var $this PagesController */
/* @var $model Pages */


$this->menu=array(	
	array('label'=>'Создать страницу', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#pages-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Управление страницами</h1>

<?php echo CHtml::link('Расширенный поиск','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'pages-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id' => array(
            'name' => 'id',
            'headerHtmlOptions' => array('width' => 30),
        ),
		'categories_id' => array(
            'name' => 'categories_id',
            'value' => '$data->categories->title',
            'filter' => Categories::all(),
        ),
		'title',		
		'created' => array(
            'name' => 'created',
            'value' => 'date("j.m.Y M:i", $date->created)',
            'filter' => false,
        ),
		'status' => array(
            'name' => 'status',
            'value' => '($data->status == 1)?"Доступно":"Скрыто"',
            'filter' => array('0'=>'Скрыто','1'=>'Доступно'),
        ),
		array(
			'class'=>'CButtonColumn',
                        'viewButtonUrl' => 'CHtml::normalizeUrl(array("/pages/view", "id" => $data->id))',
		),
	),
)); ?>
