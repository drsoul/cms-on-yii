
<div class="view">

    <b><?php echo CHtml::encode($data->getAttributeLabel('guest')); ?>:</b>
    <?php echo CHtml::encode($data->guest); ?>
    <br />
    
    <? if($data->users_id != false): ?>
    
    <b><?php echo CHtml::encode($data->getAttributeLabel('users_id')); ?>:</b>    
    <?php echo CHtml::encode($data->users->username); ?>
    <br />
    <? endif; ?>
    <b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
    <?php echo CHtml::encode(date("j.m.Y H:i", $data->created)); ?>
    <br />    

    <b><?php echo CHtml::encode($data->getAttributeLabel('content')); ?>:</b>
    <?php echo CHtml::encode($data->content); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
    <?php echo CHtml::encode($data->created); ?>
    <br />

</div>