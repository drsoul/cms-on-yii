<?php
$this->breadcrumbs=array(
    'Категория: ' . $model->categories->title => array('id' => $model->categories->id),
    $model->title,
);

echo '<H3>' . $model->title . '</H3>';
echo 'Создано: ' . date('j.m.Y H:i', $model->created);
echo '<hr/>';
echo $model->content;
?>

<?php if(Yii::app()->user->hasFlash('comment')): ?>

<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('comment'); ?>
</div>

<?php else: ?>
<?php echo $this->renderPartial('newComment', array('model' => $newComment)); ?>
<?php endif; ?>

<?php $this->widget('zii.widgets.CListView', array(
    'dataProvider'=>Comments::all($model->id),
    'itemView'=>'_viewComment',
)); ?>